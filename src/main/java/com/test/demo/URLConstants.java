package com.test.demo;

public class URLConstants {
	 private URLConstants() {
		 
	 }
    public static final String SELLER_EMAIL_SEND_OTP = "/seller/email/sendOtp";
    public static final String SELLER_EMAIL_VALIDATE_OTP = "/seller/email/validateOtp";
    public static final String SELLER_EMAIL_RESEND_OTP = "/seller/email/resendOtp";
    
    public static final String SELLER_PHONE_SEND_OTP = "/seller/phone/sendOtp";
    public static final String SELLER_PHONE_VALIDATE_OTP = "/seller/phone/validateOtp";
    public static final String SELLER_PHONE_RESEND_OTP = "/seller/phone/resendOtp";
    
    public static final String LOGIN = "/login";
    public static final String SIGNUP_BUYER = "/signup/buyer";
    public static final String SIGNUP_OTP_VALIDATE = "/signup/otp/validate";
    public static final String SIGNUP_SET_PASS ="/signup/setPassword";
    public static final String SIGNUP_OTP_RESEND ="/signup/otp/resend";
    public static final String SIGNUP_SELLER ="/signup/seller";
    
    public static final  String SMS_API = "/send";
	public static final  String EMAIL_API = "/email/mail";

	public static final String FCM_TOKEN = "/fcmtoken";
	

   
    
}
