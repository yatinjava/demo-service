package com.test.demo.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthrnticationEntryPoint extends OAuth2AuthenticationEntryPoint {


	private final CustomOAuth2SecurityExceptionHandler oAuth2SecurityExceptionHandler = new CustomOAuth2SecurityExceptionHandler();

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {
		oAuth2SecurityExceptionHandler.doHandle(request, response, authException);
	}

	
}
