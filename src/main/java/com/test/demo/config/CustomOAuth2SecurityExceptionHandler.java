package com.test.demo.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.error.DefaultOAuth2ExceptionRenderer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.OAuth2ExceptionRenderer;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomOAuth2SecurityExceptionHandler {

  private final WebResponseExceptionTranslator exceptionTranslator = new DefaultWebResponseExceptionTranslator();

  private final OAuth2ExceptionRenderer exceptionRenderer = new DefaultOAuth2ExceptionRenderer();

  private final HandlerExceptionResolver handlerExceptionResolver = new DefaultHandlerExceptionResolver();

  /**
  * This is basically what {@link org.springframework.security.oauth2.provider.error.AbstractOAuth2SecurityExceptionHandler#doHandle(HttpServletRequest, HttpServletResponse, Exception)} does.
  */
  public  void doHandle(HttpServletRequest request, HttpServletResponse response, Exception authException)
			throws IOException, ServletException {
		try {
			ResponseEntity<Map<Object, Object>> result = exceptionTranslator.translate(authException);
			result = enhanceResponse(result, authException);
			exceptionRenderer.handleHttpEntityResponse(result, new ServletWebRequest(request, response));
			response.flushBuffer();
		}
		catch (ServletException e) {
			if (handlerExceptionResolver.resolveException(request, response, this, e) == null) {
				throw e;
			}
		}
		catch (IOException | RuntimeException e) {
			throw e;
		}
		catch (Exception e) {
			throw new ServletException(e);
		}
	}

  protected ResponseEntity<Map<Object, Object>> enhanceResponse(ResponseEntity<Map<Object, Object>> result,
			Exception authException) {
	     ObjectMapper mapper = new ObjectMapper();
	     
	    HashMap<Object, Object> map = mapper.convertValue(result.getBody(), HashMap.class);
	  	map.put("responseCode", 401);
	  	map.put("responseMessage", "Unauthorized");
	  	map.remove("error_description");
	  	map.remove("error");
	  	return new ResponseEntity<>(map, result.getStatusCode());
	}
}