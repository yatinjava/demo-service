package com.test.demo.constants;

public final class UrlConstants {

	private UrlConstants(){}

	public static final String GET_BOOKS = "//library/book";
	public static final String AUTH_EMAIL_UPDATE = "/users/updateEmail";
	public static final String MARK_BOOK_AS_FAVORITE = "favoriteBook/user/{userId}/book/{bookId}";


}
