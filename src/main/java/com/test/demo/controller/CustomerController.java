package com.test.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.test.demo.response.Response;
import com.test.demo.service.CustomerService;
import com.test.demo.service.CustomerTransactionService;
import com.test.demo.utils.AbstractResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiResponses({@ApiResponse(code = 200, message = "Successful."),
        @ApiResponse(code = 204, message = "Successful, no content."),
        @ApiResponse(code = 400, message = "Bad request. Validation failure."),
        @ApiResponse(code = 201, message = "Entity created"),
        @ApiResponse(code = 404, message = "Entity not found"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 500, message = "Internal Server Error")
})
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController extends BaseController{

	@Autowired
	CustomerService customerService;
	
	
	@Autowired
	CustomerTransactionService customerTransactionService;
	
	/*
	   Get the customer portfolio details based on customer account number
	   input parameter
	   Customer Account Number-Long
	*/
	@GetMapping("customers/{accountNumber}/portfolioDetails")
	 @ApiOperation("Get Customer portfolio details")
	public ResponseEntity<Object> getCustomerTransaction(@PathVariable("accountNumber") @Min(1) Long accountNumber){
		Response portfolioDtoObj=null;
		if(accountNumber!=null && accountNumber>0)
			portfolioDtoObj=customerTransactionService.getCustomerTransaction(accountNumber);
		
		return new ResponseEntity<>(portfolioDtoObj,portfolioDtoObj.getHttpStatus());
	}
	
	
	@GetMapping("/echo")
	public String echoLogin() {
		
		return "echoCustomersLogin";

	}
	
	@GetMapping(value ="/{customerId}/accounts")
	public ResponseEntity<? extends AbstractResponse> getAccountsByCustomerId(
			@Min(1)
			@PathVariable Long customerId,
			 HttpServletRequest request) {
		return customerService.getAccountsByCustomerId(customerId);
		
		
	}
	
}
