package com.test.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Table(name = "investment_account", schema = "stockinvestmentdb")
@Entity
@Setter
@Getter
@NoArgsConstructor
public class InvestmentAccount implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_number")
	private Long accountNumber;
	
	
	@NotNull
	@Column(name = "account_balance")
	private Double accountBalance;

	@NotNull
	@Column(name = "account_date")
	private Date accountDate;

	@NotNull
	@Column(name = "account_type")
	private String accountType;

	@NotNull
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "customer_id ")
	private Customer customer;

}
