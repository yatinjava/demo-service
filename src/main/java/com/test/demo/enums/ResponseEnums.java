package com.test.demo.enums;

import java.util.ArrayList;
import java.util.List;

import com.test.demo.constants.BeepConstants;
import com.test.demo.exception.AuthResponse;
import com.test.demo.exception.AuthServerException;
import com.test.demo.exception.FieldError;
import com.test.demo.response.InvalidField;
import com.test.demo.response.ResponseCode;

public enum ResponseEnums implements EnumType  {
	SUCCESS(1000, "Success") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	FAILURE(2000, "Failure") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	CREATED(1100, "CREATED") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_CLIENT(1201, "INVALID_CLIENT") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	UNAUTHORIZED_CLIENT(1202, "UNAUTHORIZED_CLIENT") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_GRANT(1203, "INVALID_GRANT") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_SCOPE(1204, "INVALID_SCOPE") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_TOKEN(1205, "INVALID_TOKEN") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_REQUEST(1206, "INVALID_REQUEST") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	REDIRECT_URI_MISMATCH(1207, "REDIRECT_URI_MISMATCH") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	UNSUPPORTED_GRANT_TYPE(1208, "UNSUPPORTED_GRANT_TYPE") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	UNSUPPORTED_RESPONSE_TYPE(1209, "UNSUPPORTED_RESPONSE_TYPE") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	ACCESS_DENIED(1210, "ACCESS_DENIED") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	OAUTH2_EXCEPTION(1211, "OAUTH2_EXCEPTION") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	AUTHENTICATION_EXCEPTION(1212, "AUTHENTICATION_EXCEPTION") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	MISSING_SERVLET_REQUEST_PARAMETER(1301, "MISSING_SERVLET_REQUEST_PARAMETER") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	HTTP_MEDIATYPE_NOTSUPPORTED(1302, "HTTP_MEDIATYPE_NOTSUPPORTED") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	METHOD_ARGUMENT_NOTVALID(1303, "METHOD_ARGUMENT_NOTVALID") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			List<InvalidField> invalidFields = new ArrayList<>();
			for(FieldError error : authResponse.getTrace()) {
				InvalidField invalidField = new InvalidField();
				invalidField.setField(error.getField());
				invalidField.setMessage(error.getMessage());
				invalidFields.add(invalidField);
			}
			return returnResponse(httpStatus, "Invalid Parameter",invalidFields);
		}
	},
	SERVICE_EXCEPTION(1304, "SERVICE_EXCEPTION") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	CONSTRAINT_VIOLATION(1305, "CONSTRAINT_VIOLATION") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	HTTP_MESSAGE_NOTREADABLE(1306, "HTTP_MESSAGE_NOTREADABLE") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	HTTP_MESSAGE_NOTWRITABLE(1307, "HTTP_MESSAGE_NOTWRITABLE") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	NOHANDLER_FOUND(1308, "NOHANDLER_FOUND") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	DATAINTEGRITY_VIOLATION(1309, "DATAINTEGRITY_VIOLATION") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	METHOD_ARGUMENTTYPE_MISMATCH(1310, "METHOD_ARGUMENTTYPE_MISMATCH") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	REQUEST_REJECTED(1311, "REQUEST_REJECTED") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	NUMBER_FORMAT(1312, "NUMBER_FORMAT") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	ENTITY_NOTFOUND(1401, "ENTITY_NOTFOUND") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INVALID_ARGUMENT(1402, "INVALID_ARGUMENT") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	},
	INTERNAL_SERVER(500, "INTERNAL_SERVER") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(ResponseCode.INTERNALSERVER, BeepConstants.COMMON_ERROR_MSG, null);
		}
	},
	INTERNAL_SERVER_ERROR(1501, "INTERNAL_SERVER_ERROR") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, BeepConstants.COMMON_ERROR_MSG, null);
		}
	},
	RESOURCE_ACCESS_ERROR(1502, "RESOURCE_ACCESS_ERROR") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, BeepConstants.COMMON_ERROR_MSG, null);
		}
	},UNAUTHORIZED(401, "UNAUTHORIZED") {
		@Override
		protected AuthServerException convertResponse(AuthResponse authResponse, int httpStatus) {
			return returnResponse(httpStatus, authResponse.getMessage(), null);
		}
	};

	private final int id;
	private final String name;
	
	private ResponseEnums(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

    @Override
	public String getName() {
		return this.name;
	}
    
	public static ResponseEnums getFromId(int id) {
		for (ResponseEnums status : values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		return ResponseEnums.INTERNAL_SERVER;
	}

	private static AuthServerException returnResponse(int code, String message, List<InvalidField> data) {
		if(data != null) {
			return new AuthServerException(code, message,data);
		}
		return new AuthServerException(code, message);
	}
	protected abstract AuthServerException convertResponse(AuthResponse authResponse, int httpStatus);

	public AuthServerException authResponse(AuthResponse authResponse, int httpStatus) {
		return convertResponse(authResponse, httpStatus);
	}
}
