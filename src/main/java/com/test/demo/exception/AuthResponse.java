package com.test.demo.exception;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * @author emxcel
 *
 */
public class AuthResponse {

	private Integer code;
	private String message;
	private List<FieldError> trace = Collections.emptyList();
	private Object data = StringUtils.EMPTY;
	
	public AuthResponse(Integer code, String message, Object data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public AuthResponse() {
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<FieldError> getTrace() {
		return trace;
	}

	public void setTrace(List<FieldError> trace) {
		this.trace = trace;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
