package com.test.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmailAlreadyExistsException extends CoreException{

	private static final long serialVersionUID = 1L;

	public EmailAlreadyExistsException(String errorCode)
	{
		super(errorCode);
	}

	public EmailAlreadyExistsException(String errorCode , String message)
	{
		super(errorCode,message);
	}
}