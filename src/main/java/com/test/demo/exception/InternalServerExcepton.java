package com.test.demo.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * @author emxcel
 *
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerExcepton extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Integer errorCode;

	public InternalServerExcepton(String message) {
		super(message);
	}

	public InternalServerExcepton(String message,Integer errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public InternalServerExcepton(String message, Throwable cause) {
		super(message, cause);
	}

	public InternalServerExcepton(Throwable cause) {
		super(cause);
	}

	public Integer getErrorCode() {
		return errorCode;
	}
}
