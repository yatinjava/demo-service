package com.test.demo.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * @author emxcel
 *
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidArgumentExcepton extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Integer errorCode;

	public InvalidArgumentExcepton(String message) {
		super(message);
	}

	public InvalidArgumentExcepton(String message,Integer errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public InvalidArgumentExcepton(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidArgumentExcepton(Throwable cause) {
		super(cause);
	}

	public Integer getErrorCode() {
		return errorCode;
	}
}
