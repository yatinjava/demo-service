package com.test.demo.http;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class HttpUtil {
	
	HttpUtil()
	{
		
	}

    public static Object restCall(String url, RestTemplate restTemplate, String token, Object object, HttpMethod method, MediaType mediaType) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, token);
        HttpEntity<Object> entityReq = new HttpEntity<>(object, httpHeaders);
        httpHeaders.setContentType(mediaType);
        return restTemplate.exchange(url, method, entityReq, Object.class).getBody();
    }
    public static Object restCall(String url, RestTemplate restTemplate, Object object, HttpMethod method, HttpHeaders httpHeaders) {

    	HttpEntity<Object> entityReq = new HttpEntity<>(object, httpHeaders);
        return restTemplate.exchange(url, method, entityReq, Object.class).getBody();
    }
}
