package com.test.demo.logger;

import org.apache.log4j.Logger;

public class LoggerService {

	private static final Logger INFO = Logger.getLogger("info");
	private static final Logger ERROR = Logger.getLogger("error");
	private static final Logger DEBUG = Logger.getLogger("debug");
	private static final Logger EXCEPTION = Logger.getLogger("exception");
	private static final String NO_IDENTIFICATION = "No Identification";	

	private LoggerService(){
	}
	/*
	
	private static void info(String className, String transactionName, String identificationValue, String message) {
		process(Notification.create(LoggerType.INFO, className, transactionName, identificationValue, message), INFO);		
	}

	
	public static void info(String className, String transactionName, String message) {
		info(className, transactionName, NO_IDENTIFICATION, message);
	}

	private static void error(String className, String transactionName, String identificationValue, String message) {
		process(Notification.create(LoggerType.ERROR, className, transactionName, identificationValue, message), ERROR);		
	}

	
	public static void error(String className, String transactionName, String message) {
		error(className, transactionName, NO_IDENTIFICATION, message);
	}

	
	private static void debug(String className, String transactionName, String identificationValue, String message) {
		process(Notification.create(LoggerType.DEBUG, className, transactionName, identificationValue, message), DEBUG);		
	}

	
	public static void debug(String className, String transactionName, String message) {
		debug(className, transactionName, NO_IDENTIFICATION, message);
	}
	
	
	public static void exception(String className, String transactionName, String identificationValue, Throwable throwable) {
		EXCEPTION.info(Notification.create(className, transactionName, identificationValue), throwable);
	}
	
	
	public static void exception(String className, String transactionName, Throwable throwable) {
		exception(className, transactionName, NO_IDENTIFICATION, throwable);
	}
	
	
	private static void process(Notification notification, Logger logger) {
		if (logger != null) {
			logger.info(notification);
		}
	}*/
}