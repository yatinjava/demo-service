package com.test.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.test.demo.entity.Customer;
import com.test.demo.utils.AbstractResponse;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InvestmentAccountDtoWrapper extends AbstractResponse{
	
	List<InvestmentAccountDto> investmentAccounts;



}
