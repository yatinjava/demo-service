package com.test.demo.model;

import java.util.List;

public class PortfolioDto {

	private double totalStockValue;
	private double balanceAmount;
	private List<TransactionDto> transactionDto;

	public double getTotalStockValue() {
		return totalStockValue;
	}

	public void setTotalStockValue(double totalStockValue) {
		this.totalStockValue = totalStockValue;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public List<TransactionDto> getTransactionDto() {
		return transactionDto;
	}

	public void setTransactionDto(List<TransactionDto> transactionDto) {
		this.transactionDto = transactionDto;
	}

	
}
