package com.test.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.test.demo.entity.Transaction;
	
@Repository
public interface CustomerTransactionRepo extends CrudRepository<Transaction,Long>{

	
}
