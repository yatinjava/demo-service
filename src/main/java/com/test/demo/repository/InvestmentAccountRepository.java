package com.test.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.demo.entity.InvestmentAccount;


@Repository
public interface InvestmentAccountRepository extends JpaRepository<InvestmentAccount, Long> {

	
	
	
	Optional<List<InvestmentAccount>> findByCustomerCustomerId(Long customerId);
	
	
}
