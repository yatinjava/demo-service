package com.test.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.test.demo.entity.Portfolio;

@Repository
public interface PortfolioRepository extends CrudRepository<Portfolio,Long>{

	@Query(value="select stock_price from stock_details where stock_name= :stockName",nativeQuery=true)
	public double getLatestStockPrice(@Param("stockName") String stockName);
}
