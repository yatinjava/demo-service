package com.test.demo.service;

import org.springframework.http.ResponseEntity;

import com.test.demo.utils.AbstractResponse;

public interface CustomerService {

	ResponseEntity<? extends AbstractResponse> getAccountsByCustomerId(Long customerId);

}
