package com.test.demo.service;

import com.test.demo.model.PortfolioDto;
import com.test.demo.response.Response;

public interface CustomerTransactionService {

	public Response getCustomerTransaction(Long accountNumber);
}
