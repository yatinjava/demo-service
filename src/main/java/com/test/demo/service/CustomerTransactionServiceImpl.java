package com.test.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.demo.response.DataResponse;
import com.test.demo.response.Response;
import com.test.demo.constants.BeepConstants;
import com.test.demo.entity.Portfolio;
import com.test.demo.entity.Transaction;
import com.test.demo.model.PortfolioDto;
import com.test.demo.model.TransactionDto;
import com.test.demo.repository.PortfolioRepository;

@Service
public class CustomerTransactionServiceImpl implements CustomerTransactionService{

	@Autowired
	PortfolioRepository portfolioRepository;
	@Override
	public Response getCustomerTransaction(Long accountNumber) {
		// TODO Auto-generated method stub
		
		Optional<Portfolio> portfolioObj=portfolioRepository.findById(accountNumber);
		if(portfolioObj.isPresent()) {
			Portfolio portfolio=portfolioObj.get();
			List<Transaction> transactionObject=portfolio.getTransaction();
			double totalStockValue = 0;
			List<TransactionDto> transactionDtoList=new ArrayList<>();
			PortfolioDto portfolioDto=new PortfolioDto();
			for(Transaction transaction:transactionObject) {
				TransactionDto obj=new TransactionDto();
				String instrumentName=transaction.getInstrumentName();
				Double stockPrice=  portfolioRepository.getLatestStockPrice(instrumentName);
				int quantity=transaction.getQuantity();
				obj.setPrice(stockPrice);
				obj.setQuantity(quantity);
				obj.setStockName(instrumentName);
				obj.setValue(stockPrice*quantity);
				totalStockValue=totalStockValue+(stockPrice*quantity);
				transactionDtoList.add(obj);
			}
			portfolioDto.setTotalStockValue(totalStockValue);
			portfolioDto.setBalanceAmount(portfolio.getAccountBalance());
			portfolioDto.setTransactionDto(transactionDtoList);
			Map<String, Object> response = new HashMap<>();
	        response.put("portfolioDetail-",portfolioDto);
	        return new DataResponse(HttpStatus.OK, HttpStatus.OK.value(), BeepConstants.PORTFOLIO_DETAIL_FOUND, response);
		}
		return new Response(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), BeepConstants.PORTFOLIO_DETAIL_NOT_FOUND);
	}

}
