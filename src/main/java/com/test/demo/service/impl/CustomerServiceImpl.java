package com.test.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.test.demo.entity.InvestmentAccount;
import com.test.demo.model.InvestmentAccountDto;
import com.test.demo.model.InvestmentAccountDtoWrapper;
import com.test.demo.repository.InvestmentAccountRepository;
import com.test.demo.service.CustomerService;
import com.test.demo.utils.AbstractResponse;
import com.test.demo.utils.AppConstants;
import com.test.demo.utils.StatusResponse;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	InvestmentAccountRepository investmentAccountRepository;
	
    private static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);


	@Override
	public ResponseEntity<? extends AbstractResponse> getAccountsByCustomerId(Long customerId) {
		// TODO Auto-generated method stub
		
		InvestmentAccountDtoWrapper investmentAccountDtoWrapper = new InvestmentAccountDtoWrapper();
		List<InvestmentAccountDto> investmentAccounts = new ArrayList<InvestmentAccountDto>();
		Optional<List<InvestmentAccount>> investmentAccountList= investmentAccountRepository.findByCustomerCustomerId(customerId);
		 
		if(investmentAccountList.isPresent()) {
			
			investmentAccountList.get().forEach(investmentAccount -> {
				InvestmentAccountDto investmentAccountDto = new InvestmentAccountDto();
				investmentAccountDto.setAccountNumber(investmentAccount.getAccountNumber());
				investmentAccountDto.setAccountType(investmentAccount.getAccountType());
				investmentAccountDto.setAccountBalance(investmentAccount.getAccountBalance());
				investmentAccounts.add(investmentAccountDto);
			});
			investmentAccountDtoWrapper.setInvestmentAccounts(investmentAccounts);
		}else {
			return new ResponseEntity<>(new StatusResponse(AppConstants.INVALID_CUSTOMER_ID, "Customer Id Not Exist"),
					HttpStatus.NOT_FOUND);
		} 
		return new ResponseEntity<>(investmentAccountDtoWrapper, HttpStatus.OK);
		
		
		
	}

}
