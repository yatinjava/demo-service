package com.test.demo.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.test.demo.exception.DateParseException;
import com.test.demo.exception.ErrorCode;
import lombok.extern.log4j.Log4j2;

@Log4j2
public final class BeepUtils {
	
	private BeepUtils() {
	}

	static final Random r = new Random();

	public static String generateRandomNumber6Digit() {
		return String.format("%06d", r.nextInt(999999));
	}

	private static final ThreadLocal<SimpleDateFormat> FORMAT_DD_MM_YYYY = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy");
		}
	};

	public static Date convertStringToDate(String date) {
		try {
			return FORMAT_DD_MM_YYYY.get().parse(date);
		} catch (ParseException exception) {
			//log.info(exception);
			throw new DateParseException(ErrorCode.Common.SOMETHING_WENT_WRONG);
		}
	}
	
	public static String convertDateToString(Date date) {
		 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
		return dateFormat.format(date);
	}
	
	public static boolean matchRegex(String regex, String value) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}
}
