package com.test.demo.utils;

import java.util.Map;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

	private JsonUtils(){
	}
    
	private static ObjectMapper mapper = new ObjectMapper();
    
	static{
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, Boolean.FALSE);
    }
	
	
    @SuppressWarnings("unchecked")
	public static <T> T toObject(final byte[] json, Class<?> aClass)  {
    	try {
    		  return (T) mapper.readValue(json, aClass);
		} catch (Exception e) {
			//LoggerService.exception(BeepConstants.UTIL, BeepConstants.INVALID_JSON, e);
			return null;
		}
    }
    
    public static String toJsonFromMap(Map<String, ? extends Object> map) {
	    try{
	    	return mapper.writeValueAsString(map);
	    } catch (Exception e) {
	    	//LoggerService.exception(BeepConstants.UTIL, BeepConstants.INVALID_JSON, e);
			return null;
		}
    }
    
    public static String toJson(Object aClass) {
	    try{
	    	return mapper.writeValueAsString(aClass);
	    } catch (Exception e) {
	    	//LoggerService.exception(BeepConstants.UTIL, BeepConstants.INVALID_JSON, e);
			return null;
		}
    }
    
    public static Map<String, Object> toMap(String jsonString) {
	    try{
	    	ObjectMapper mapper = new ObjectMapper();
	    	return mapper.readValue(jsonString, Map.class);
	    } catch (Exception e) {
	    	//LoggerService.exception(BeepConstants.UTIL, BeepConstants.INVALID_JSON, e);
			return null;
		}
    }
}
