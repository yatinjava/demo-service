package com.test.demo.utils;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.test.demo.exception.InternalServerExcepton;

/**
 * @author emxcel
 *
 */
@Component
public class RestTemplateUtil {


	

	public Object fallback(String url, RestTemplate restTemplate,  Object object, HttpMethod method) {
		throw new InternalServerExcepton("Something went wrong.");
	}

}
