package com.test.demo.config;


import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 */
@EnableWebMvc
@TestPropertySource("classpath:application.properties")
public abstract class AbstractBaseTest {

	protected MockMvc mockMvc;

	@Mock
	protected MessageSource messageSource;

	@Mock
	protected HttpServletRequest request;

	@Mock
	protected PasswordEncoder passwordEncoder;

	@Mock
	protected Environment env;

	@Mock
	protected ModelMapper mapper;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
}
