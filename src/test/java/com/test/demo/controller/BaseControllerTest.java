package com.test.demo.controller;

import com.test.demo.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


@WebMvcTest(SpringJUnit4ClassRunner.class)
class BaseControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(BaseControllerTest.class);

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private BaseController baseController;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private Exception e;

    @Test
    void testResourceNotFoundException() {
        ResourceNotFoundException resourceNotFoundException = new ResourceNotFoundException("recource not ound");
        ResponseEntity<Map<String, Object>> map = baseController.resourcesNotFoundHandler(resourceNotFoundException, httpServletRequest, e);
        String errorCode = resourceNotFoundException.getErrorCode();
        messageSource.getMessage(errorCode, null, Locale.US);
        Map<String, Object> message = getMessage(errorCode);
        assertEquals(404, map.getStatusCodeValue());
        assertEquals(3, message.size());
    }

    private Map<String, Object> getMessage(String errorCode) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", messageSource.getMessage(errorCode, null, Locale.US));
        map.put("statusCode", HttpStatus.NOT_FOUND.value());
        map.put("error", HttpStatus.NOT_FOUND);
        logger.info(messageSource.getMessage(errorCode, null, Locale.US), HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND);
        return map;
    }
}
