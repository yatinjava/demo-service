package com.test.demo.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.test.demo.config.CORSFilter;
import com.test.demo.constants.BeepConstants;
import com.test.demo.model.PortfolioDto;
import com.test.demo.model.TransactionDto;
import com.test.demo.response.DataResponse;
import com.test.demo.service.CustomerTransactionService;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerTransactionService customerTransactionService;

    @InjectMocks
    private CustomerController customerController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).addFilter(new CORSFilter()).build();
    }

    /* Call the proper API with proper Data */
    @Test
    public void getCustomerTransaction_FoundTest() throws Exception {
    	Map<String, Object> response = new HashMap<>();
        response.put("portfolioDetail-",getCustomerTransaction());
        DataResponse dataResponse = new DataResponse(HttpStatus.OK, HttpStatus.OK.value(),
                BeepConstants.PORTFOLIO_DETAIL_FOUND, response);
        when(customerTransactionService.getCustomerTransaction(1l)).thenReturn(dataResponse);
        mockMvc.perform(get("/customers/1/portfolioDetails").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andDo(print());

    }

   


    private PortfolioDto getCustomerTransaction() {
    	TransactionDto transactionObj1=new TransactionDto();
    	transactionObj1.setPrice(200.00);
    	transactionObj1.setQuantity(20);
    	transactionObj1.setStockName("HDFC");
    	transactionObj1.setValue(2000.00);
    	
    	TransactionDto transactionObj2=new TransactionDto();
    	transactionObj2.setPrice(250.00);
    	transactionObj2.setQuantity(10);
    	transactionObj2.setStockName("SBI");
    	transactionObj2.setValue(2500.00);
    	List<TransactionDto> transactionList=new ArrayList<>();
    	transactionList.add(transactionObj1);
    	transactionList.add(transactionObj2);
    	PortfolioDto PortfolioDtoObj = new PortfolioDto();
    	PortfolioDtoObj.setTotalStockValue(4500.00);
    	PortfolioDtoObj.setBalanceAmount(1500.00);
    	PortfolioDtoObj.setTransactionDto(transactionList);
        return PortfolioDtoObj;
    }
}
